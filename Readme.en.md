# Poll: surveys for TYPO3

## Features in the basic version

* Create unlimited numbers of polls with any number of questions
* Many different answer options consisting of text and images
* Plugin for displaying polls including data entry options and optional display of results on completion
* Plugin for displaying poll teasers (e.g. on your home page or sidebar)
* Plugin for displaying lists of all polls
* Separate plugin to display poll results
* Simple polls/surveys (multiple answers per question; one or more answers)
* Prevent repeat participation by setting cookie

## Features in the PRO version

* Backend module for managing polls and poll results
* Export poll results as CSV file
* User-defined answers: users can enter their own answers to questions
* Reset poll

## Roadmap
* Statistics
* Spam protection using Captcha
* Dashboard widgets
* Send results by email

### Links

*   [TYPO3 Poll Product details][link-typo3-poll-product-details]
*   [TYPO3 Poll Documentation][link-typo3-poll-documentation]



[link-typo3-poll-product-details]: https://www.coding.ms/typo3-extensions/typo3-poll/ "TYPO3 Poll Product details"
[link-typo3-poll-documentation]: https://www.coding.ms/documentation/typo3-poll/ "TYPO3 Poll Documentation"

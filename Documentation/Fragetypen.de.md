# Fragetypen und Layout für Antwortmöglichkeiten

## Basisversion

Folgende Fragetypen können in der Basisversion verwendet werden:

### Einfachauswahl

Im Backend werden Antwortmöglichkeiten vorgegeben. Nur eine Antwort kann ausgewählt werden.

### Mehrfachauswahl

Im Backend werden Antwortmöglichkeiten vorgegeben. Mehrere Antworten können ausgewählt werden.

## PRO-Version

In der PRO-Version können zusätzlich folgende Fragetypen verwendet werden:

### Einfachauswahl inkl. benutzerdefinierter Antworten

Im Backend werden Antwortmöglichkeiten vorgegeben. Nur eine Antwort kann ausgewählt werden. Jede
Antwortmöglichkeit kann optional mit einem Feld für Texteingaben durch den Benutzer ergänzt werden.

### Mehrfachauswahl

Im Backend werden Antwortmöglichkeiten vorgegeben. Mehrere Antworten können ausgewählt werden. Jede
Antwortmöglichkeit kann optional mit einem Feld für Texteingaben durch den Benutzer ergänzt werden.

### Benutzerdefinierte Einzelantwort

Es ist nur eine benutzerdefinierte Antwort möglich. **Achtung:** Hier muss trotzdem ein Antwort-Datensatz erstellt werden!

## Layout für Antwortmöglichkeiten

Die Antwortmöglichkeiten können in verschiedenen Layouts dargestellt werden:

* Normal: Radiobox für Einfachauswahl, Checkbox für Mehrfachauswahl
* Skala: z. B. für Antwortmöglichkeiten nach Schulnoten oder 1-10, nur für Fragetyp "Einfachauswahl"
* Select: Dropdown-Feld, nur für Fragetyp "Einfachauswahl"
* Mehrzeilige Benutzerantwort: Mehrzeiliges Eingabefeld für die Eingabe eines
  Freitextes, nur für Fragetyp "Benutzerdefinierte Einzelauswahl"

Es können zusätzliche CSS-Klassen vergeben werden, um die Gestaltung anzupassen, ohne die
Fluid-Templates modifizieren zu müssen.

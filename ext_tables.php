<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'poll',
    'Poll',
    'LLL:EXT:poll/Resources/Private/Language/locallang.xlf:tx_poll_domain_model_poll.show_poll'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'poll',
    'PollTeaser',
    'LLL:EXT:poll/Resources/Private/Language/locallang.xlf:tx_poll_domain_model_poll.show_poll_teaser'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'poll',
    'PollResult',
    'LLL:EXT:poll/Resources/Private/Language/locallang.xlf:tx_poll_domain_model_poll.show_poll_result'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'poll',
    'PollList',
    'LLL:EXT:poll/Resources/Private/Language/locallang.xlf:tx_poll_domain_model_poll.show_poll_list'
);
//
// register svg icons: identifier and filename
$iconsSvg = [
    'apps-pagetree-folder-contains-polls' => 'Resources/Public/Icons/iconmonstr-checkbox-20.svg',
    'icon-content-plugin-polls-poll' => 'Resources/Public/Icons/iconmonstr-checkbox-20.svg',
    'actions-poll-tickets' => 'Resources/Public/Icons/iconmonstr-language-10.svg',
];
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
foreach ($iconsSvg as $identifier => $path) {
    $iconRegistry->registerIcon(
        $identifier,
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        ['source' => 'EXT:poll/' . $path]
    );
}
//
// Table configuration arrays
$tables = [
    'tx_poll_domain_model_poll' => [
        'csh' => 'EXT:poll/Resources/Private/Language/locallang_csh_poll.xlf'
    ],
    'tx_poll_domain_model_pollquestion' => [
        'csh' => 'EXT:poll/Resources/Private/Language/locallang_csh_pollquestion.xlf'
    ],
    'tx_poll_domain_model_pollquestionanswer' => [
        'csh' => 'EXT:poll/Resources/Private/Language/locallang_csh_pollquestionanswer.xlf'
    ],
    'tx_poll_domain_model_pollticket' => [
        'csh' => 'EXT:poll/Resources/Private/Language/locallang_csh_pollticket.xlf'
    ],
    'tx_poll_domain_model_pollticketanswer' => [
        'csh' => 'EXT:poll/Resources/Private/Language/locallang_csh_pollticketanswer.xlf'
    ]
];
foreach ($tables as $table => $data) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr($table, $data['csh']);
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages($table);
}

<?php

namespace CodingMs\Poll\Domain\Model;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\DomainObject\AbstractValueObject;
use TYPO3\CMS\Extbase\Annotation as Extbase;

/**
 *
 * @package poll
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class PollTicketAnswer extends AbstractEntity
{

    /**
     * @var string
     */
    protected $userAnswer = '';

    /**
     * @var \CodingMs\Poll\Domain\Model\PollQuestion
     * @Extbase\ORM\Lazy
     */
    protected $pollQuestion;

    /**
     * @var \CodingMs\Poll\Domain\Model\PollQuestionAnswer
     */
    protected $pollQuestionAnswer;

    /**
     * @return string $userAnswer
     */
    public function getUserAnswer()
    {
        return $this->userAnswer;
    }

    /**
     * @param string $userAnswer
     * @return void
     */
    public function setUserAnswer($userAnswer)
    {
        $this->userAnswer = $userAnswer;
    }

    /**
     * @return \CodingMs\Poll\Domain\Model\PollQuestion $pollQuestion
     */
    public function getPollQuestion()
    {
        return $this->pollQuestion;
    }

    /**
     * @param \CodingMs\Poll\Domain\Model\PollQuestion $pollQuestion
     * @return void
     */
    public function setPollQuestion(\CodingMs\Poll\Domain\Model\PollQuestion $pollQuestion)
    {
        $this->pollQuestion = $pollQuestion;
    }

    /**
     * @return \CodingMs\Poll\Domain\Model\PollQuestionAnswer $pollQuestionAnswer
     */
    public function getPollQuestionAnswer()
    {
        return $this->pollQuestionAnswer;
    }

    /**
     * @param \CodingMs\Poll\Domain\Model\PollQuestionAnswer $pollQuestionAnswer
     * @return void
     */
    public function setPollQuestionAnswer(\CodingMs\Poll\Domain\Model\PollQuestionAnswer $pollQuestionAnswer)
    {
        $this->pollQuestionAnswer = $pollQuestionAnswer;
    }

}

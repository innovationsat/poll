# Available plugins

Here is information about available plugins for this extension.

## Display poll

Displays a poll.

This plugin has the following settings:

Name                  |Description
----------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Show poll             |Select which poll should be displayed.

## Display poll teaser

Displays the poll teaser for the selected poll.

This plugin has the following settings:

Name                       |Description
---------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Teaser Title               |Enter the teaser title.
Single view page           |Set the page for the poll. A ``Show poll`` plugin must be available on the page.
Show poll                  |Select which survey should be displayed.

## Display poll result

Displays the result of a poll. This plugin is meant to be a target page after finishing a poll, so that you are able to display the result on a separate page. This plugin does not have any settings at this time.

## Display poll list

Displays a list of all polls in the assigned data store.

This plugin has the following settings:

Name                       |Description
---------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
List title                 |Enter the title for the list.
Single view page           |Set the page for the poll. A ``Show poll`` plugin must be available on the page.
List offset                |Set how many entries are to be skipped.
List order field           |Set the field which should be used to sort the list. To sort the entries manually, select "manual".
List order direction       |Select whether to do an "ascending" or "descending" sort on the field above.

<?php

namespace CodingMs\Poll\Domain\Model;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Annotation as Extbase;

/**
 *
 * @package poll
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class PollQuestionAnswer extends AbstractEntity
{

    /**
     * @var integer
     */
    protected $sorting;

    /**
     * @var string
     * @Extbase\Validate("NotEmpty")
     */
    protected $answer;

    /**
     * @var integer
     */
    protected $answerCount;

    /**
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @Extbase\ORM\Lazy
     */
    protected $image;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var boolean
     */
    protected $useUserAnswer;

    /**
     * @var string
     */
    protected $userAnswerPlaceholder;

    /**
     * @var string
     */
    protected $tempUserAnswer;

    /**
     * @return integer $sorting
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * @param integer $sorting
     * @return void
     */
    public function setSorting($sorting)
    {
        $this->sorting = $sorting;
    }

    /**
     * @return string $answer
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * @param string $answer
     * @return void
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
    }

    /**
     * @return integer $answerCount
     */
    public function getAnswerCount()
    {
        return $this->answerCount;
    }

    /**
     * @param integer $answerCount
     * @return void
     */
    public function setAnswerCount($answerCount)
    {
        $this->answerCount = $answerCount;
    }

    /**
     * @param integer $clickCount
     * @return void
     */
    public function incrementAnswerCount()
    {
        $this->answerCount++;
    }

    /**
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    public function getImage()
    {
        if (!is_object($this->image)) {
            return null;
        } else {
            if ($this->image instanceof \TYPO3\CMS\Extbase\Persistence\Generic\LazyLoadingProxy) {
                $this->image->_loadRealInstance();
            }
        }
        return $this->image->getOriginalResource();
    }

    /**
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return boolean $useUserAnswer
     */
    public function getUseUserAnswer()
    {
        return $this->useUserAnswer;
    }

    /**
     * @param boolean $useUserAnswer
     * @return void
     */
    public function setUseUserAnswer($useUserAnswer)
    {
        $this->useUserAnswer = $useUserAnswer;
    }

    /**
     * @return string $userAnswerPlaceholder
     */
    public function getUserAnswerPlaceholder()
    {
        return $this->userAnswerPlaceholder;
    }

    /**
     * @param string $userAnswerPlaceholder
     * @return void
     */
    public function setUserAnswerPlaceholder($userAnswerPlaceholder)
    {
        $this->userAnswerPlaceholder = $userAnswerPlaceholder;
    }

    /**
     * @return string $tempUserAnswer
     */
    public function getTempUserAnswer()
    {
        return $this->tempUserAnswer;
    }

    /**
     * @param string $tempUserAnswer
     * @return void
     */
    public function setTempUserAnswer($tempUserAnswer)
    {
        $this->tempUserAnswer = $tempUserAnswer;
    }

}

<?php

namespace CodingMs\Poll\Domain\Model;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Annotation as Extbase;

/**
 *
 * @package poll
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class PollTicket extends AbstractEntity
{

    /**
     * @var \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
     */
    protected $frontendUser;

    /**
     * @var \CodingMs\Poll\Domain\Model\Poll
     * @Extbase\ORM\Lazy
     */
    protected $poll;

    /**
     * @var boolean
     */
    protected $isFinished;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Poll\Domain\Model\PollQuestion>
     */
    protected $pollQuestion;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Poll\Domain\Model\PollTicketAnswer>
     */
    protected $pollTicketAnswer;

    /**
     * @return void
     */
    public function __construct()
    {
        $this->initStorageObjects();
    }

    /**
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->pollQuestion = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->pollTicketAnswer = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * @return \TYPO3\CMS\Extbase\Domain\Model\FrontendUser $frontendUser
     */
    public function getFrontendUser()
    {
        return $this->frontendUser;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Domain\Model\FrontendUser $frontendUser
     * @return void
     */
    public function setFrontendUser($frontendUser)
    {
        $this->frontendUser = $frontendUser;
    }

    /**
     * @return boolean $isFinished
     */
    public function getIsFinished()
    {
        return $this->isFinished;
    }

    /**
     * @return boolean $isFinished
     */
    public function isFinished()
    {
        return $this->isFinished;
    }

    /**
     * @param boolean $isFinished
     * @return void
     */
    public function setIsFinished($isFinished)
    {
        $this->isFinished = $isFinished;
    }

    /**
     * @return \CodingMs\Poll\Domain\Model\Poll $poll
     */
    public function getPoll()
    {
        return $this->poll;
    }

    /**
     * @param \CodingMs\Poll\Domain\Model\Poll $poll
     * @return void
     */
    public function setPoll(\CodingMs\Poll\Domain\Model\Poll $poll)
    {
        $this->poll = $poll;
    }

    /**
     * @param \CodingMs\Poll\Domain\Model\PollQuestion $pollQuestion
     * @return void
     */
    public function addPollQuestion(\CodingMs\Poll\Domain\Model\PollQuestion $pollQuestion)
    {
        $this->pollQuestion->attach($pollQuestion);
    }

    /**
     * @param \CodingMs\Poll\Domain\Model\PollQuestion $pollQuestionToRemove The PollQuestion to be removed
     * @return void
     */
    public function removePollQuestion(\CodingMs\Poll\Domain\Model\PollQuestion $pollQuestionToRemove)
    {
        $this->pollQuestion->detach($pollQuestionToRemove);
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Poll\Domain\Model\PollQuestion> $pollQuestion
     */
    public function getPollQuestion()
    {
        return $this->pollQuestion;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Poll\Domain\Model\PollQuestion> $pollQuestion
     * @return void
     */
    public function setPollQuestion(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $pollQuestion)
    {
        $this->pollQuestion = $pollQuestion;
    }

    /**
     * @param \CodingMs\Poll\Domain\Model\PollTicketAnswer $pollTicketAnswer
     * @return void
     */
    public function addPollTicketAnswer(\CodingMs\Poll\Domain\Model\PollTicketAnswer $pollTicketAnswer)
    {
        $this->pollTicketAnswer->attach($pollTicketAnswer);
    }

    /**
     * @param \CodingMs\Poll\Domain\Model\PollTicketAnswer $pollTicketAnswerToRemove The PollTicketAnswer to be removed
     * @return void
     */
    public function removePollTicketAnswer(\CodingMs\Poll\Domain\Model\PollTicketAnswer $pollTicketAnswerToRemove)
    {
        $this->pollTicketAnswer->detach($pollTicketAnswerToRemove);
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Poll\Domain\Model\PollTicketAnswer> $pollTicketAnswer
     */
    public function getPollTicketAnswer()
    {
        return $this->pollTicketAnswer;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Poll\Domain\Model\PollTicketAnswer> $pollTicketAnswer
     * @return void
     */
    public function setPollTicketAnswer(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $pollTicketAnswer)
    {
        $this->pollTicketAnswer = $pollTicketAnswer;
    }

}

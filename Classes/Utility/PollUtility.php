<?php

namespace CodingMs\Poll\Utility;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Poll\Domain\Model\PollQuestionAnswer;
use CodingMs\Poll\Domain\Model\PollTicket;
use CodingMs\Poll\Domain\Model\PollTicketAnswer;
use CodingMs\Poll\Domain\Repository\PollRepository;
use Exception;
use CodingMs\Poll\Domain\Repository\PollTicketRepository;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Model\FrontendUser;
use TYPO3\CMS\Extbase\Domain\Repository\FrontendUserRepository;
use CodingMs\Poll\Domain\Model\Poll;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use CodingMs\Poll\Domain\Model\PollQuestion;
use TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper;

/**
 * Poll utilities
 *
 * @package poll
 * @subpackage Utility
 */
class PollUtility
{

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @param ObjectManager $objectManager
     */
    public function injectObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @var PollRepository
     */
    protected $pollRepository;

    /**
     * @param PollRepository $pollRepository
     * @noinspection PhpUnused
     */
    public function injectPollRepository(PollRepository $pollRepository)
    {
        $this->pollRepository = $pollRepository;
    }

    /**
     * @var PollTicketRepository
     */
    protected $pollTicketRepository;

    /**
     * @param PollTicketRepository $pollTicketRepository
     * @noinspection PhpUnused
     */
    public function injectPollTicketRepository(PollTicketRepository $pollTicketRepository)
    {
        $this->pollTicketRepository = $pollTicketRepository;
    }

    /**
     * @var FrontendUserRepository
     */
    protected $frontendUserRepository;

    /**
     * @param FrontendUserRepository $frontendUserRepository
     * @noinspection PhpUnused
     */
    public function injectFrontendUserRepository(FrontendUserRepository $frontendUserRepository)
    {
        $this->frontendUserRepository = $frontendUserRepository;
    }

    /**
     * Returns if a FrontendUser has already done a poll
     *
     * @param Poll $poll Poll-Object
     * @param FrontendUser $frontendUser FrontendUser-Object
     * @return boolean
     */
    public function isPollAlreadyDoneByFrontendUser(Poll $poll, FrontendUser $frontendUser)
    {
        $pollTicket = $this->pollTicketRepository->findOneByPollAndFrontendUser($poll, $frontendUser);
        return ($pollTicket instanceof PollTicket);
    }

    /**
     * Gets the current frontend user
     *
     * @return mixed
     * @throws Exception
     */
    public function getFrontendUser()
    {
        // User is logged in?
        if (isset($GLOBALS['TSFE']->fe_user->user)) {
            $frontendUserUid = (int)$GLOBALS['TSFE']->fe_user->user['uid'];
            if ($frontendUserUid > 0) {
                $frontendUser = $this->getUserByUidWithoutPid($frontendUserUid);
                if ($frontendUser instanceof FrontendUser) {
                    return $frontendUser;
                } else {
                    throw new Exception('FrontendUser not found (uid:' . $frontendUserUid . ')');
                }
            }
        } else {
            throw new Exception('FrontendUser login required');
        }
        return null;
    }

    /**
     * @param int $uid
     * @return mixed
     * @throws \TYPO3\CMS\Extbase\Object\Exception
     */
    private function getUserByUidWithoutPid(int $uid) {
        /** @var DataMapper $dataMapper */
        $dataMapper = $this->objectManager->get(DataMapper::class);
        /** @var ConnectionPool $connectionPool */
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        /** @var QueryBuilder $queryBuilder*/
        $queryBuilder = $connectionPool->getQueryBuilderForTable('fe_users');
        $queryBuilder->select('*')
            ->from('fe_users')
            ->where($queryBuilder->expr()->eq('uid', $uid));
        return $dataMapper->map(FrontendUser::class, [$queryBuilder->execute()->fetch()])[0];
    }

    /**
     *
     * @param Poll $poll Poll-Object
     * @param FrontendUser|null $frontendUser FrontendUser-Object
     * @return bool
     * @throws Exception
     */
    public function getAlreadyParticipated(Poll $poll, FrontendUser $frontendUser = null)
    {
        $alreadyParticipated = false;
        //
        // Cookie protected and already participated
        if ($poll->getUseCookieProtection() && (boolean)$_COOKIE['pollParticipate_' . $poll->getUid()]) {
            $alreadyParticipated = true;
        }
        //
        // Poll runs in frontendUser-mode..
        if ($poll->getMode() == 'frontendUser') {
            if ($frontendUser === null) {
                throw new Exception('FrontendUser required');
            } else {
                //
                // Poll isn't editable and already done by this frontend user
                $alreadyDone = $this->isPollAlreadyDoneByFrontendUser($poll, $frontendUser);
                if (!$poll->isEditable() && $alreadyDone) {
                    $alreadyParticipated = true;
                }
            }
        }
        return $alreadyParticipated;
    }

    /**
     * Restores a saved poll ticket
     *
     * @param Poll $poll
     * @param FrontendUser|null $frontendUser
     * @return array
     */
    public function getPollTicketAnswersArray(Poll $poll, FrontendUser $frontendUser = null)
    {
        $answersArray = [];
        //
        // Only get the poll ticket, if there is already one
        /** @var PollTicket $pollTicket */
        $pollTicket = $this->pollTicketRepository->findOneByPollAndFrontendUser($poll, $frontendUser);
        if ($pollTicket instanceof PollTicket) {
            //
            // Don't restore in case of finished tickets
            if (!$pollTicket->isFinished()) {
                $pollTicketAnswers = $pollTicket->getPollTicketAnswer();
                if (!empty($pollTicketAnswers)) {
                    /** @var PollTicketAnswer $pollTicketAnswer */
                    foreach ($pollTicketAnswers as $pollTicketAnswer) {
                        $question = $pollTicketAnswer->getPollQuestion();
                        $questionKey = 'question_' . $question->getUid();
                        $questionAnswer = $pollTicketAnswer->getPollQuestionAnswer();
                        $qaid = $questionAnswer->getUid();
                        $questionAnswerKey = $questionKey . '_' . $qaid;
                        switch($question->getQuestionType()) {
                            case 'Single':
                                $answersArray[$questionKey] = $qaid;
                                break;
                            case 'SingleWithOptionalUserAnswer':
                                $answersArray[$questionKey] = $qaid;
                                $answersArray[$questionAnswerKey . '_userAnswer'] = $pollTicketAnswer->getUserAnswer();
                                break;
                            case 'Multiple':
                                $answersArray[$questionAnswerKey] = $qaid;
                                break;
                            case 'MultipleWithOptionalUserAnswer':
                                $answersArray[$questionAnswerKey] = $qaid;
                                $answersArray[$questionAnswerKey . '_userAnswer'] = $pollTicketAnswer->getUserAnswer();
                                break;
                            case 'SingleUserAnswer':
                                $answersArray[$questionKey] = $qaid;
                                $answersArray[$questionKey . '_userAnswer'] = $pollTicketAnswer->getUserAnswer();
                                break;
                        }
                    }
                    //
                }
            }
        }
        return $answersArray;
    }

    /**
     * Returns only a poll question answer uid
     * @param PollQuestion $pollQuestion
     * @param $value
     * @return int
     */
    public function getPollQuestionAnswerUid(PollQuestion $pollQuestion, $value): int
    {
        $uid = 0;
        $answers = $pollQuestion->getQuestionAnswer();
        if (!empty($answers)) {
            /** @var PollQuestionAnswer $answer */
            foreach ($answers as $answer) {
                if ($answer->getAnswer() == $value) {
                    $uid = $answer->getUid();
                    break;
                }
            }
        }
        return $uid;
    }

}

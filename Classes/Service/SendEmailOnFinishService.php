<?php

namespace CodingMs\Poll\Service;

use CodingMs\Poll\Domain\Model\PollQuestion;
use CodingMs\Poll\Domain\Model\PollQuestionAnswer;
use CodingMs\Poll\Domain\Model\PollTicket;
use CodingMs\Poll\Domain\Repository\PollQuestionAnswerRepository;
use CodingMs\Poll\Domain\Repository\PollQuestionRepository;
use CodingMs\Poll\Utility\SendMailUtility;
use CodingMs\Poll\Utility\ToolsUtility;
use Exception;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Object\ObjectManager;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Saving for questions
 *
 * @package poll
 * @subpackage Service
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class SendEmailOnFinishService
{

    /**
     * @var PollQuestionRepository
     */
    protected $pollQuestionRepository;

    /**
     * @param PollQuestionRepository $pollQuestionRepository
     * @noinspection PhpUnused
     */
    public function injectPollQuestionRepository(PollQuestionRepository $pollQuestionRepository)
    {
        $this->pollQuestionRepository = $pollQuestionRepository;
    }

    /**
     * @var PollQuestionAnswerRepository
     */
    protected $pollQuestionAnswerRepository;

    /**
     * @param PollQuestionAnswerRepository $pollQuestionAnswerRepository
     * @noinspection PhpUnused
     */
    public function injectPollQuestionAnswerRepository(PollQuestionAnswerRepository $pollQuestionAnswerRepository)
    {
        $this->pollQuestionAnswerRepository = $pollQuestionAnswerRepository;
    }

    /**
     * @var ConfigurationManagerInterface
     */
    protected $configurationManager;

    /**
     * @param ConfigurationManagerInterface $configurationManagerInterface
     * @noinspection PhpUnused
     */
    public function injectConfigurationManagerInterface(ConfigurationManagerInterface $configurationManagerInterface)
    {
        $this->configurationManagerInterface = $configurationManagerInterface;
    }

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @param ObjectManager $objectManager
     */
    public function injectObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param PollTicket $pollTicket
     * @param PollQuestion $question
     * @return bool
     */
    public function sendEmail($answers = [], $settings)
    {

        return true;

        /**
         * @todo refactor
         */

        $answerRows = [];
        if (!empty($answers)) {
            foreach ($answers as $answerKey => $answer) {
                $questionUid = (int)str_replace('question_', '', $answerKey);
                $answerRows[] = $this->getQuestionEmailRow($questionUid, $answer);
            }
        }

        // E-Mail Daten
        $emailData = [];
        $emailData['toEmail'] = $settings['result']['toEmail'];
        $emailData['toName'] = $settings['result']['toName'];
        $emailData['fromEmail'] = $settings['result']['fromEmail'];
        $emailData['fromName'] = $settings['result']['fromName'];
        $emailData['answerRows'] = $answerRows;
        //$emailData['params'] = $params;

        // E-Mailtext rendern und E-Mail versenden
        $configuration = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
        $templateName = 'PollResult';
        $templateFile = 'Email/' . $templateName . '.html';
        $templatePath = ToolsUtility::getTemplatePath($configuration['view'], 'template', $templateFile);

        $emailData = SendMailUtility::renderEmail($templatePath, $templateName, $emailData, $settings);
        return SendMailUtility::send($emailData);
    }

    protected function getQuestionEmailRow($questionUid, $answer)
    {

        $row = '';

        /** @var PollQuestion $question */
        $question = $this->pollQuestionRepository->findByIdentifier($questionUid);

        if ($question instanceof PollQuestion) {

            $headline = $question->getQuestionHeadline();
            if (trim($headline) != '') {
                $row .= "\n";
                $row .= $headline . "\n";
                $row .= "--------------\n";
            }

            $questionType = $question->getQuestionType();
            $questionPlain = ToolsUtility::convertHtmlToPlainText($question->getQuestion());

            // Answer
            if ($questionType == 'SingleUserAnswer') {
                $row .= $questionPlain . ": " . $answer . "\n";
            } else {
                if ($questionType == 'Single') {
                    /** @var PollQuestionAnswer $answerObject */
                    $answerObject = $this->getPollQuestionAnswer($answer);
                    $row .= $questionPlain . ": " . $answerObject->getAnswer() . "\n";
                }
            }

        } else {
            $row .= "Question not found (" . $questionUid . ", " . $answer . ")";
        }
        return $row;
    }

    protected function getPollQuestionAnswer($answerUid)
    {
        /** @var PollQuestionAnswer $answer */
        $answer = $this->pollQuestionAnswerRepository->findByIdentifier($answerUid);
        if ($answer instanceof PollQuestionAnswer) {
            return $answer;
        } else {
            throw new Exception('PollQuestionAnswer not found (uid: ' . $answerUid . ')');
        }
    }

}

<?php declare(strict_types=1);

namespace CodingMs\Poll\Tca;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Configuration presets for TCA fields.
 * Target version TYPO3 9.5-10.4
 *
 * @package poll
 * @subpackage Tca
 * @author Thomas Deuling <typo3@coding.ms>
 * @version 1.3.3
 */
class ConfigurationPreset
{

    /**
     * @param string $type
     * @param bool $required
     * @param bool $readonly
     * @param string $label Additional label, e.g. checkboxes.
     * @param array $options Options for select boxes, field name for slug
     * @return array
     */
    public static function get($type, $required = false, $readonly = false, $label = '', array $options=[]): array
    {
        $config = [];
        switch ($type) {
            case 'slug':
                $options['field'] = isset($options['field']) ? $options['field'] : 'title';
                if ($required) {
                    $config['eval'] .= ',required';
                }
                if((int)TYPO3_version === 8) {
                    $config = [
                        'type' => 'input',
                        'size' => 30,
                        'eval' => 'nospace,alphanum_x,lower,unique',
                    ];
                }
                else {
                    $config = [
                        'type' => 'slug',
                        'size' => 50,
                        'prependSlash' => true,
                        'generatorOptions' => [
                            'fields' => [$options['field']],
                            'prefixParentPageSlug' => true,
                            'replacements' => [
                                '/' => '-'
                            ],
                        ],
                        'fallbackCharacter' => '-',
                        'eval' => 'lower,uniqueInSite',
                    ];
                }
                break;
            case 'rte':
                $config = [
                    'type' => 'text',
                    'cols' => 40,
                    'rows' => 15,
                    'eval' => 'trim',
                    'enableRichtext' => 1,
                    'richtextConfiguration' => 'default',
                ];
                if ($required) {
                    $config['eval'] .= ',required';
                }
                break;
            case 'textareaSmall':
                $config = [
                    'type' => 'text',
                    'cols' => 40,
                    'rows' => 5,
                    'eval' => 'trim',
                ];
                if ($required) {
                    $config['eval'] .= ',required';
                }
                break;
            case 'textareaLarge':
                $config = [
                    'type' => 'text',
                    'cols' => 40,
                    'rows' => 15,
                    'eval' => 'trim',
                ];
                if ($required) {
                    $config['eval'] .= ',required';
                }
                break;
            case 'int':
                $config = [
                    'type' => 'input',
                    'size' => 4,
                    'eval' => 'int'
                ];
                if ($required) {
                    $config['eval'] .= ',required';
                }
                break;
            case 'currency':
                $config = [
                    'type' => 'input',
                    'renderType' => 'Currency',
                    'size' => 12,
                    'eval' => 'int',
                    'readOnly' => false,
                    'default' => '0',
                    'fieldInformation' => [
                        'AdditionalInfoPageTitle' => [
                            'renderType' => 'Currency',
                        ],
                    ],
                ];
                if ($required) {
                    $config['eval'] .= ',required';
                }
                break;
            case 'percent':
                $config = [
                    'type' => 'input',
                    'renderType' => 'Percent',
                    'size' => 4,
                    'eval' => 'int',
                    'default' => 0,
                    'fieldInformation' => [
                        'AdditionalInfoPageTitle' => [
                            'renderType' => 'Percent',
                        ],
                    ],
                ];
                if ($required) {
                    $config['eval'] .= ',required';
                }
                break;
            case 'string':
                $config = [
                    'type' => 'input',
                    'size' => 30,
                    'eval' => 'trim'
                ];
                if ($required) {
                    $config['eval'] .= ',required';
                }
                break;
            case 'email':
                $config = [
                    'type' => 'input',
                    'size' => 30,
                    'eval' => 'email,trim'
                ];
                if ($required) {
                    $config['eval'] .= ',required';
                }
                break;
            case 'checkbox':
                $config = [
                    'type' => 'check',
                    'items' => [
                        '1' => [
                            '0' => $label,
                        ],
                    ],
                    'default' => 0,
                ];
                break;
            case 'link':
                $config = [
                    'type' => 'input',
                    'size' => 30,
                    'softref' => 'typolink',
                    'wizards' => [
                        '_PADDING' => 2,
                        'link' => [
                            'type' => 'popup',
                            'title' => 'Link',
                            'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_link.gif',
                            'module' => [
                                'name' => 'wizard_element_browser',
                                'urlParameters' => [
                                    'mode' => 'wizard'
                                ],
                            ],
                            'JSopenParams' => 'height=600,width=500,status=0,menubar=0,scrollbars=1'
                        ],
                    ],
                ];
                break;
            case 'date':
                $config = [
                    'dbType' => 'date',
                    'type' => 'input',
                    'renderType' => 'inputDateTime',
                    'size' => 12,
                    'eval' => 'date',
                    'default' => null,
                ];
                if ($required) {
                    $config['eval'] .= ',required';
                }
                break;
            case 'dateTime':
                $config = [
                    'dbType' => 'datetime',
                    'type' => 'input',
                    'renderType' => 'inputDateTime',
                    'size' => 12,
                    'eval' => 'datetime',
                    'default' => null,
                ];
                if ($required) {
                    $config['eval'] .= ',required';
                }
                break;
            case 'select':
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'eval' => 'trim',
                    'items' => $options,
                ];
                if ($required) {
                    $config['eval'] .= ',required';
                }
                break;
            case 'fileSingle':
                $config = [
                    'type' => 'group',
                    'internal_type' => 'file',
                    'allowed' => 'md',
                    'size' => '1',
                    'maxitems' => '1',
                    'minitems' => '0',
                    'eval' => '',
                    'show_thumbs' => '1',
                    'wizards' => [
                        'suggest' => [
                            'type' => 'suggest'
                        ],
                    ],
                ];
                if ($required) {
                    $config['minitems'] = 1;
                }
                break;
            case 'fileCollectionSingleInline':
                $config = [
                    'type' => 'inline',
                    'foreign_table' => 'sys_file_collection',
                    'minitems' => 0,
                    'maxitems' => 1,
                    'appearance' => [
                        'collapseAll' => 0,
                        'levelLinksPosition' => 'top',
                        'showSynchronizationLink' => 1,
                        'showPossibleLocalizationRecords' => 1,
                        'showAllLocalizationLink' => 1
                    ],
                ];
                if ($required) {
                    $config['minitems'] = 1;
                }
                break;
            case 'imageSingleAltTitle':
                $config =  \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('image');
                $config['maxitems'] = 1;
                $config['overrideChildTca'] = [
                    'columns' => [
                        'uid_local' => [
                            'config' => [
                                'appearance' => [
                                    'elementBrowserAllowed' => 'png,jpg,jpeg',
                                    'elementBrowserType' => 'file',
                                ],
                            ],
                        ],
                    ],
                    'types' => [
                        '0' => [
                            'showitem' => 'title,alternative,
                           --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => 'title,alternative,
                           --palette--;;filePalette'
                        ],
                    ]
                ];
                if ($required) {
                    $config['minitems'] = 1;
                }
                break;
            case 'imageSingleTitleDescription':
                $config =  \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('image');
                $config['maxitems'] = 1;
                $config['overrideChildTca'] = [
                    'columns' => [
                        'uid_local' => [
                            'config' => [
                                'appearance' => [
                                    'elementBrowserAllowed' => 'png,jpg,jpeg',
                                    'elementBrowserType' => 'file',
                                ],
                            ],
                        ],
                    ],
                    'types' => [
                        '0' => [
                            'showitem' => 'title,description,
                           --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => 'title,description,
                           --palette--;;filePalette'
                        ],
                    ]
                ];
                if ($required) {
                    $config['minitems'] = 1;
                }
                break;
        }
        if ($readonly) {
            $config['readOnly'] = true;
        }
        return $config;
    }

    /**
     * @param string $type
     * @param string $table
     * @param string $extensionKey
     * @return array
     */
    public static function full(string $type, string $table='', string $extensionKey='') {
        $config = [];
        switch ($type) {
            case 'sys_language_uid':
                $config = [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
                    'config' => [
                        'type' => 'select',
                        'renderType' => 'selectSingle',
                        'foreign_table' => 'sys_language',
                        'foreign_table_where' => 'ORDER BY sys_language.title',
                        'default' => -1,
                        'items' => [
                            ['LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages', -1],
                            ['LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.default_value', 0]
                        ],
                    ],
                ];
                if((int)TYPO3_version > 8) {
                    $config['label'] = 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language';
                    $config['config']['items'] = [
                        ['LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages', -1],
                        ['LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.default_value', 0]
                    ];
                }
                break;
            case 'l10n_parent':
                $config = [
                    'displayCond' => 'FIELD:sys_language_uid:>:0',
                    'exclude' => 1,
                    'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
                    'config' => [
                        'type' => 'select',
                        'items' => [
                            ['', 0],
                        ],
                        'foreign_table' => $table,
                        'foreign_table_where' => 'AND ' . $table . '.pid=###CURRENT_PID### AND ' . $table . '.sys_language_uid IN (-1,0)',
                    ],
                ];
                if((int)TYPO3_version > 8) {
                    $config['label'] = 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent';
                }
                break;
            case 'l10n_diffsource':
                $config = [
                    'config' => [
                        'type' => 'passthrough',
                    ],
                ];
                break;
            case 't3ver_label':
                $config = [
                    'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
                    'config' => [
                        'type' => 'input',
                        'size' => 30,
                        'max' => 255,
                    ]
                ];
                if((int)TYPO3_version > 8) {
                    $config['label'] = 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel';
                }
                break;
            case 'hidden':
                $config = [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
                    'config' => [
                        'type' => 'check',
                    ],
                ];
                if((int)TYPO3_version > 8) {
                    $config['label'] = 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hidden';
                }
                break;
            case 'starttime':
                $config = [
                    'exclude' => 1,
                    'l10n_mode' => 'mergeIfNotBlank',
                    'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
                    'config' => [
                        'type' => 'input',
                        'renderType' => 'inputDateTime',
                        'size' => 13,
                        'max' => 20,
                        'eval' => 'datetime,int',
                        'checkbox' => 0,
                        'default' => 0,
                    ],
                ];
                if((int)TYPO3_version > 8) {
                    $config['label'] = 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime';
                }
                break;
            case 'endtime':
                $config = [
                    'exclude' => 1,
                    'l10n_mode' => 'mergeIfNotBlank',
                    'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
                    'config' => [
                        'type' => 'input',
                        'renderType' => 'inputDateTime',
                        'size' => 13,
                        'max' => 20,
                        'eval' => 'datetime,int',
                        'checkbox' => 0,
                        'default' => 0,
                    ],
                ];
                if((int)TYPO3_version > 8) {
                    $config['label'] = 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime';
                }
                break;
            case 'information':
                $config = [
                    'exclude' => 0,
                    'label' => '',
                    'config' => [
                        'type' => 'user',
                        'renderType' => 'Information',
                        'userFunc' => 'CodingMs\\' . $extensionKey . '\\Tca\\InformationRow->renderField',
                    ]
                ];
                break;
            case 'support':
                $config = [
                    'exclude' => 0,
                    'label' => '',
                    'config' => [
                        'type' => 'user',
                        'renderType' => 'Support',
                        'userFunc' => 'CodingMs\\' . $extensionKey . '\\Tca\\SupportRow->renderField'
                    ]
                ];
                break;
        }
        return $config;
    }

    /**
     * @param string $type
     * @return mixed
     */
    public static function label(string $type) {
        $label = [
            'tab_general' => 'LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general',
            'tab_language' => 'LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language',
            'tab_access' => 'LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access',
            'tab_notes' => 'LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes',
            'tab_extended' => 'LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
        ];
        if((int)TYPO3_version > 8) {
            $label['tab_general'] = 'LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general';
            $label['tab_language'] = 'LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language';
            $label['tab_access'] = 'LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access';
            $label['tab_notes'] = 'LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes';
            $label['tab_extended'] = 'LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        }
        return isset($label[$type]) ? $label[$type] : $type;
    }

}

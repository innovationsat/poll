<?php

namespace CodingMs\Poll\Domain\Model;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\Generic\LazyLoadingProxy;
use TYPO3\CMS\Extbase\Annotation as Extbase;
use Exception;

/**
 *
 *
 * @package poll
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Poll extends AbstractEntity
{

    /**
     * @var string
     * @Extbase\Validate("NotEmpty")
     */
    protected $title;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @Extbase\ORM\Lazy
     */
    protected $image;

    /**
     * @var string
     */
    protected $mode;

    /**
     * @var boolean
     */
    protected $useCookieProtection;

    /**
     * @var boolean
     */
    protected $useCaptchaProtection;

    /**
     * @var boolean
     */
    protected $withoutPollTicket;

    /**
     * @var boolean
     */
    protected $editable;

    /**
     * @var boolean
     */
    protected $sendEmailOnFinish;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Poll\Domain\Model\PollQuestion>
     */
    protected $pollQuestion;

    /**
     * @return void
     */
    public function __construct()
    {
        $this->initStorageObjects();
    }

    /**
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->pollQuestion = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    public function getImage()
    {
        if (!is_object($this->image)) {
            return null;
        } else {
            if ($this->image instanceof LazyLoadingProxy) {
                $this->image->_loadRealInstance();
            }
        }
        return $this->image->getOriginalResource();
    }

    /**
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     * @noinspection PhpUnused
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string $mode
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param string $mode
     * @return void
     * @noinspection PhpUnused
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    /**
     * @return boolean $useCookieProtection
     */
    public function getUseCookieProtection()
    {
        return $this->useCookieProtection;
    }

    /**
     * @param boolean $useCookieProtection
     * @return void
     * @noinspection PhpUnused
     */
    public function setUseCookieProtection($useCookieProtection)
    {
        $this->useCookieProtection = $useCookieProtection;
    }

    /**
     * @return boolean $useCaptchaProtection
     */
    public function getUseCaptchaProtection()
    {
        return $this->useCaptchaProtection;
    }

    /**
     * @param boolean $useCaptchaProtection
     * @return void
     * @noinspection PhpUnused
     */
    public function setUseCaptchaProtection($useCaptchaProtection)
    {
        $this->useCaptchaProtection = $useCaptchaProtection;
    }

    /**
     * @return boolean $withoutPollTicket
     * @noinspection PhpUnused
     */
    public function getWithoutPollTicket()
    {
        return $this->withoutPollTicket;
    }

    /**
     * @return boolean $withoutPollTicket
     */
    public function isWithoutPollTicket()
    {
        return $this->withoutPollTicket;
    }

    /**
     * @param boolean $withoutPollTicket
     * @return void
     * @noinspection PhpUnused
     */
    public function setWithoutPollTicket($withoutPollTicket)
    {
        $this->withoutPollTicket = $withoutPollTicket;
    }

    /**
     * @return boolean $editable
     * @noinspection PhpUnused
     */
    public function getEditable()
    {
        return $this->editable;
    }

    /**
     * @return boolean $editable
     */
    public function isEditable()
    {
        return $this->editable;
    }

    /**
     * @param boolean $editable
     * @return void
     * @noinspection PhpUnused
     */
    public function setEditable($editable)
    {
        $this->editable = $editable;
    }

    /**
     * @return boolean $sendEmailOnFinish
     * @noinspection PhpUnused
     */
    public function getSendEmailOnFinish()
    {
        return $this->sendEmailOnFinish;
    }

    /**
     * @return boolean $sendEmailOnFinish
     */
    public function isSendEmailOnFinish()
    {
        return $this->sendEmailOnFinish;
    }

    /**
     * @param boolean $sendEmailOnFinish
     * @return void
     * @noinspection PhpUnused
     */
    public function setSendEmailOnFinish($sendEmailOnFinish)
    {
        $this->sendEmailOnFinish = $sendEmailOnFinish;
    }

    /**
     * @param \CodingMs\Poll\Domain\Model\PollQuestion $pollQuestion
     * @return void
     * @noinspection PhpUnused
     */
    public function addPollQuestion(\CodingMs\Poll\Domain\Model\PollQuestion $pollQuestion)
    {
        $this->pollQuestion->attach($pollQuestion);
    }

    /**
     * @param \CodingMs\Poll\Domain\Model\PollQuestion $pollQuestionToRemove The PollQuestion to be removed
     * @return void
     * @noinspection PhpUnused
     */
    public function removePollQuestion(\CodingMs\Poll\Domain\Model\PollQuestion $pollQuestionToRemove)
    {
        $this->pollQuestion->detach($pollQuestionToRemove);
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Poll\Domain\Model\PollQuestion> $pollQuestion
     */
    public function getPollQuestion()
    {
        return $this->pollQuestion;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Poll\Domain\Model\PollQuestion> $pollQuestion
     * @return void
     * @noinspection PhpUnused
     */
    public function setPollQuestion(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $pollQuestion)
    {
        $this->pollQuestion = $pollQuestion;
    }

    /**
     * @return array $resultArray
     * @throws Exception
     */
    public function getPollResultArray()
    {
        $resultArray = array();
        //
        // Build array structure for result data
        // ..and calculate answer count
        $questions = $this->getPollQuestion();
        if (!empty($questions)) {
            /** @var PollQuestion $pollQuestion */
            foreach ($questions as $pollQuestion) {
                // Questions
                $pollQuestionPosition = $pollQuestion->getUid();
                $resultArray[$pollQuestionPosition]['pollQuestion'] = $pollQuestion;
                $resultArray[$pollQuestionPosition]['answerCount'] = 0;
                // Answers
                $answers = $pollQuestion->getQuestionAnswer();
                if (!empty($answers)) {
                    /** @var PollQuestionAnswer $pollAnswer */
                    foreach ($answers as $pollAnswer) {
                        $pollAnswerPosition = $pollAnswer->getUid();
                        $pollTicketInline = '(SELECT uid FROM tx_poll_domain_model_pollticket WHERE poll = ' . $this->getUid() . ' AND deleted = 0)';
                        $resultArray[$pollQuestionPosition]['result'][$pollAnswerPosition]['pollAnswer'] = $pollAnswer;
                        if (!$this->isWithoutPollTicket()) {
                            /** @var ConnectionPool $connectionPool */
                            $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
                            /** @var QueryBuilder $queryBuilder */
                            $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_poll_domain_model_pollticketanswer');
                            $queryBuilder->count('uid')
                                ->from('tx_poll_domain_model_pollticketanswer')
                                ->where(
                                    $queryBuilder->expr()->in('poll_ticket', $pollTicketInline)
                                )
                                ->andWhere(
                                    $queryBuilder->expr()->eq('poll_question_answer', $pollAnswer->getUid())
                                );

                            /**
                             * @todo collect custom answers
                             */

                            $resultArray[$pollQuestionPosition]['result'][$pollAnswerPosition]['pollAnswerCount'] = $queryBuilder->execute()->fetchColumn(0);
                            $resultArray[$pollQuestionPosition]['answerCount'] += $queryBuilder->execute()->fetchColumn(0);
                        } else {
                            $resultArray[$pollQuestionPosition]['result'][$pollAnswerPosition]['pollAnswerCount'] = $pollAnswer->getAnswerCount();
                            $resultArray[$pollQuestionPosition]['answerCount'] += $pollAnswer->getAnswerCount();
                        }
                    }
                }
            }
        }
        //
        // Calculate percentages
        if (!empty($resultArray)) {
            foreach ($resultArray as $pollQuestionPosition => $pollQuestion) {
                if ($resultArray[$pollQuestionPosition]['answerCount'] > 0) {
                    $answerAmount = 0;
                    foreach ($resultArray[$pollQuestionPosition]['result'] as $pollAnswerPosition => $pollAnswerCount) {
                        $oneAnswerPercent = 100 / $resultArray[$pollQuestionPosition]['answerCount'];
                        $answerPercent = $resultArray[$pollQuestionPosition]['result'][$pollAnswerPosition]['pollAnswerCount'] * $oneAnswerPercent;
                        $answerPercent = (float)number_format((float)$answerPercent, 1);
                        if (!isset($resultArray[$pollQuestionPosition]['result'][$pollAnswerPosition]['pollAnswerPercent'])) {
                            $resultArray[$pollQuestionPosition]['result'][$pollAnswerPosition]['pollAnswerPercent'] = $answerPercent;
                            $answerAmount += $answerPercent;
                        }
                    }
                }
            }
        }
        return $resultArray;
    }

}

# RealURL

Configure RealURL for this extension by simply adding the following to the RealURL configuration:

    'postVarSets' => array(
        '_DEFAULT' => array(

            // EXT:poll start
            'umfrage' => array(
                array(
                    'GETvar' => 'tx_poll_poll[controller]',
                    'valueMap' => array(
                        'daten' => 'Poll',
                    ),
                    'noMatch' => 'bypass',
                ),
                array(
                    'GETvar' => 'tx_poll_poll[action]',
                    'valueMap' => array(
                        'anzeigen' => 'showPoll',
                        'abmelden' => 'unsubscribe',
                        'anmeldung-bestaetigen' => 'subscribeConfirm',
                        'abmeldung-bestaetigen' => 'unsubscribeConfirm',
                    ),
                    'noMatch' => 'bypass',
                ),
				array(
					'GETvar' => 'tx_poll_poll[poll]',
					'lookUpTable' => array(
					'table' => 'tx_poll_domain_model_poll',
					'id_field' => 'uid',
					'alias_field' => 'title',
					'addWhereClause' => ' AND NOT deleted',
					'useUniqueCache' => 1,
					'useUniqueCache_conf' => array(
						'strtolower' => 1,
						'spaceCharacter' => '-',
						),
					),
				),
            ),
            // EXT:poll end

        ),
    ),



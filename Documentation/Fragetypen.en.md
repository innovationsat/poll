# Question types and layouts for answer options

## Basic version

The following question types can be used in the basic version:

### Single selection

Answer options are specified in the backend. Only one answer can be selected.

### Multiple selection

Answer options are specified in the backend. Multiple answers can be selected.

## PRO version

The following extra question types are available in the PRO version:

### Simple selection including user-defined answers

Answer options are specified in the backend. Only one answer can be selected. Answer options can have extra fields added for text input by the user.

### Multiple selection

Answer options are specified in the backend. Multiple answers can be selected. Answer options can have extra fields added for text input by the user.

### User-defined single answer

In this case only one user-defined answer is possible. **Attention:** You need to create an answer record in the question!

## Layout for answer options

The answer options can be presented in different layouts:

* Normal: radio box for single selection, checkbox for multiple selection
* Scale: e.g. answer options for school grades or 1-10, only available for "single selection" question type
* Select: dropdown box, only available for "single selection" question type
* Multi-line user response: multi-line input field for entering free text, only available for "User-defined individual selection" question type

Additional CSS classes can be assigned to adapt the design without needing to modify Fluid templates.

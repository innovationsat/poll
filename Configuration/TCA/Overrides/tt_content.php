<?php
defined('TYPO3_MODE') or die();
//
// Include flex forms
$flexForms = ['Poll', 'PollList', 'PollTeaser'];
foreach ($flexForms as $pluginName) {
    $pluginSignature = 'poll_' . strtolower($pluginName);
    $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
    $flexForm = 'FILE:EXT:poll/Configuration/FlexForms/' . $pluginName . '.xml';
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, $flexForm);
}

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['poll_poll'] = 'recursive,select_key,pages';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['poll_pollteaser'] = 'recursive,select_key,pages';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['poll_pollresult'] = 'recursive,select_key,pages';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['poll_polllist'] = 'recursive,select_key,pages';

<?php

namespace CodingMs\Poll\Service\Save;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Poll\Domain\Model\PollQuestionAnswer;
use CodingMs\Poll\Domain\Model\PollTicket;
use CodingMs\Poll\Domain\Model\PollQuestion;
use CodingMs\Poll\Domain\Model\PollTicketAnswer;
use Exception;

/**
 * Saving for questions
 *
 * @package poll
 * @subpackage Service
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class SaveMultiple extends AbstractSave
{

    /**
     * Saves a question
     *
     * @param PollTicket $pollTicket
     * @param array $answersArray
     * @param PollQuestion $question
     * @return PollTicket Updated poll ticket
     * @throws Exception
     */
    public function save(PollTicket $pollTicket, array $answersArray, PollQuestion $question)
    {
        $qid = $question->getUid();
        $this->answerKey = 'question_' . $qid;
        //
        // Update an existing answer
        if ($this->getPollTicketAnswer($pollTicket, $question)) {
            $pollQuestionAnswers = $question->getQuestionAnswer();
            if (!empty($pollQuestionAnswers)) {
                foreach ($pollQuestionAnswers as $questionAnswer) {
                    $qaid = $questionAnswer->getUid();
                    $qaidKey = 'question_' . $qid . '_' . $qaid;
                    if (isset($answersArray[$qaidKey]) && !empty($answersArray[$qaidKey])) {
                        //
                        // Insert, when not selected yet
                        $alreadySelected = false;
                        /** @var PollTicketAnswer $pollTicketAnswer */
                        foreach ($pollTicket->getPollTicketAnswer() as $pollTicketAnswer) {
                            $pollQuestionAnswer = $pollTicketAnswer->getPollQuestionAnswer();
                            if ($pollQuestionAnswer->getUid() === $qaid) {
                                $alreadySelected = true;
                            }
                        }
                        if (!$alreadySelected) {
                            $pollTicket = $this->insert($pollTicket, [$qaidKey => $qaid], $question);
                            //
                            // rewind, because current position might has changed
                            $pollQuestionAnswers->rewind();
                        }
                    } else {
                        //
                        // Remove a previous selected answer
                        /** @var PollTicketAnswer $pollTicketAnswer */
                        foreach ($pollTicket->getPollTicketAnswer() as $pollTicketAnswer) {
                            $pollQuestionAnswer = $pollTicketAnswer->getPollQuestionAnswer();
                            if ($pollQuestionAnswer->getUid() === $qaid) {
                                $pollTicket->removePollTicketAnswer($pollTicketAnswer);
                            }
                        }
                    }
                }
            }
        } else {
            // Insert new answers
            $pollQuestionAnswers = $question->getQuestionAnswer();
            if (!empty($pollQuestionAnswers)) {
                $insertAnswers = [];
                /** @var PollQuestionAnswer $pollQuestionAnswer */
                foreach ($pollQuestionAnswers as $pollQuestionAnswer) {
                    $qaidKey = 'question_' . $qid . '_' . $pollQuestionAnswer->getUid();
                    if (isset($answersArray[$qaidKey])) {
                        if (!empty($answersArray[$qaidKey])) {
                            $insertAnswers[$qaidKey] = $pollQuestionAnswer->getUid();
                        }
                    } else {
                        throw new Exception($qaidKey . ' not found');
                    }
                }
                $pollTicket = $this->insert($pollTicket, $insertAnswers, $question);
            } else {
                throw new Exception('Question (uid: ' . $qid . ') don\'t have any answers!');
            }
        }
        return $pollTicket;
    }

}

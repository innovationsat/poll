<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

$extKey = 'poll';
$table = 'tx_poll_domain_model_pollticketanswer';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

return [
    'ctrl' => [
        'title' => $lll,
        'label' => 'poll_question',
        'label_alt' => 'poll_question_answer',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,
        'versioningWS' => 2,
        'versioning_followPages' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'hideTable' => 1,
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'iconfile' => 'EXT:poll/Resources/Public/Icons/iconmonstr-language-10.svg'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, user_answer, poll_question, poll_question_answer',
    ],
    'types' => [
        '1' => ['showitem' => '
            poll_question,
            user_answer,
            poll_question_answer'
        ],
    ],
    'palettes' => [
        '1' => ['showitem' => ''],
    ],
    'columns' => [
        'sys_language_uid' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::full('l10n_parent', $table, $extKey),
        'l10n_diffsource' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::full('t3ver_label'),
        'hidden' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::full('hidden'),
        'starttime' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::full('starttime'),
        'endtime' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::full('endtime'),
        'user_answer' => [
            'exclude' => 0,
            'label' => $lll . '.user_answer',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'readOnly' => 1,
                'default' => '',
            ],
        ],
        'poll_question' => [
            'exclude' => 0,
            'label' => $lll . '.poll_question',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_poll_domain_model_pollquestion',
                'size' => 1,
                'maxitems' => 1,
                'minitems' => 1,
                'readOnly' => 1,
            ],
        ],
        'poll_question_answer' => [
            'exclude' => 0,
            'label' => $lll . '.poll_question_answer',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_poll_domain_model_pollquestionanswer',
                'size' => 1,
                'maxitems' => 1,
                'minitems' => 1,
                'readOnly' => 1,
            ],
        ],
    ],
];

<?php

namespace CodingMs\Poll\Domain\Model;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Annotation as Extbase;

/**
 *
 *
 * @package poll
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class PollQuestion extends AbstractEntity
{

    /**
     * @var integer
     */
    protected $sorting;

    /**
     * @var string
     */
    protected $questionHeadline;

    /**
     * @var string
     * @Extbase\Validate("NotEmpty")
     */
    protected $question;

    /**
     * @var string
     * @Extbase\Validate("NotEmpty")
     */
    protected $questionType;

    /**
     * @var string
     */
    protected $questionLayout;

    /**
     * @var string
     */
    protected $additionalCssClass;

    /**
     * @var boolean
     */
    protected $showHrBefore;

    /**
     * @var boolean
     */
    protected $optional;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Poll\Domain\Model\PollQuestionAnswer>
     */
    protected $questionAnswer;

    /**
     * @var string
     */
    protected $questionAnswerDefault;

    /**
     * @return void
     */
    public function __construct()
    {
        $this->initStorageObjects();
    }

    /**
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->questionAnswer = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * @return integer $sorting
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * @param integer $sorting
     * @return void
     */
    public function setSorting($sorting)
    {
        $this->sorting = $sorting;
    }

    /**
     * @return string $questionHeadline
     */
    public function getQuestionHeadline()
    {
        return $this->questionHeadline;
    }

    /**
     * @param string $questionHeadline
     * @return void
     */
    public function setQuestionHeadline($questionHeadline)
    {
        $this->questionHeadline = $questionHeadline;
    }

    /**
     * @return string $question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param string $question
     * @return void
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    }

    /**
     * @return string $questionType
     */
    public function getQuestionType()
    {
        return $this->questionType;
    }

    /**
     * @param string $questionType
     * @return void
     */
    public function setQuestionType($questionType)
    {
        $this->questionType = $questionType;
    }

    /**
     * @return string $questionLayout
     */
    public function getQuestionLayout()
    {
        return $this->questionLayout;
    }

    /**
     * @param string $questionLayout
     * @return void
     */
    public function setQuestionLayout($questionLayout)
    {
        $this->questionLayout = $questionLayout;
    }

    /**
     * @return string $additionalCssClass
     */
    public function getAdditionalCssClass()
    {
        return $this->additionalCssClass;
    }

    /**
     * @param string $additionalCssClass
     * @return void
     */
    public function setAdditionalCssClass($additionalCssClass)
    {
        $this->additionalCssClass = $additionalCssClass;
    }

    /**
     * @return boolean $showHrBefore
     */
    public function getShowHrBefore()
    {
        return $this->showHrBefore;
    }

    /**
     * @return boolean $showHrBefore
     */
    public function isShowHrBefore()
    {
        return $this->showHrBefore;
    }

    /**
     * @param boolean $showHrBefore
     * @return void
     */
    public function setShowHrBefore($showHrBefore)
    {
        $this->showHrBefore = $showHrBefore;
    }

    /**
     * @return bool
     */
    public function getOptional(): bool
    {
        return $this->optional;
    }

    /**
     * @return bool
     */
    public function isOptional(): bool
    {
        return $this->optional;
    }

    /**
     * @param bool $optional
     */
    public function setOptional(bool $optional): void
    {
        $this->optional = $optional;
    }

    /**
     * @return string $questionAnswerDefault
     */
    public function getQuestionAnswerDefault()
    {
        return $this->questionAnswerDefault;
    }

    /**
     * @param string $questionAnswerDefault
     * @return void
     */
    public function setQuestionAnswerDefault($questionAnswerDefault)
    {
        $this->questionAnswerDefault = $questionAnswerDefault;
    }

    /**
     * @param \CodingMs\Poll\Domain\Model\PollQuestionAnswer $questionAnswer
     * @return void
     */
    public function addQuestionAnswer(\CodingMs\Poll\Domain\Model\PollQuestionAnswer $questionAnswer)
    {
        $this->questionAnswer->attach($questionAnswer);
    }

    /**
     * @param \CodingMs\Poll\Domain\Model\PollQuestionAnswer $questionAnswerToRemove The PollQuestionAnswer to be removed
     * @return void
     */
    public function removeQuestionAnswer(\CodingMs\Poll\Domain\Model\PollQuestionAnswer $questionAnswerToRemove)
    {
        $this->questionAnswer->detach($questionAnswerToRemove);
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Poll\Domain\Model\PollQuestionAnswer> $questionAnswer
     */
    public function getQuestionAnswer()
    {
        return $this->questionAnswer;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Poll\Domain\Model\PollQuestionAnswer> $questionAnswer
     * @return void
     */
    public function setQuestionAnswer(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $questionAnswer)
    {
        $this->questionAnswer = $questionAnswer;
    }

}

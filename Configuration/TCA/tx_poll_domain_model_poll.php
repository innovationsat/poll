<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

$extKey = 'poll';
$table = 'tx_poll_domain_model_poll';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

return [
    'ctrl' => [
        'title' => $lll,
        'label' => 'title',
        'tstamp' => 'tstamp',
        'sortby' => 'sorting',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,
        'versioningWS' => 2,
        'versioning_followPages' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'iconfile' => 'EXT:poll/Resources/Public/Icons/iconmonstr-help-6.svg'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, description, image, mode, use_cookie_protection, without_poll_ticket, editable, poll_question',
    ],
    'types' => [
        '1' => [
            'showitem' => '
            title,
            description,
            mode,
            --palette--;' . $lll . '.palette_protection;protection,
            without_poll_ticket,
            editable,
        --div--;' . $lll . '.tab_questions,
            poll_question,
        --div--;' . $lll . '.tab_image,
            image,
        --div--;' . $lll . '.tab_language,
            sys_language_uid;;;;1-1-1,
            l10n_parent,
            l10n_diffsource,
        --div--;' . $lll . '.tab_access,
            hidden;;1,
            starttime,
            endtime'
        ],
    ],
    'palettes' => [
        'protection' => ['showitem' => 'use_cookie_protection'],
    ],
    'columns' => [
        'sys_language_uid' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::full('l10n_parent', $table, $extKey),
        'l10n_diffsource' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::full('t3ver_label'),
        'hidden' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::full('hidden'),
        'starttime' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::full('starttime'),
        'endtime' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::full('endtime'),
        'title' => [
            'exclude' => 0,
            'label' => $lll . '.title',
            'config' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::get('string', true),
        ],
        'description' => [
            'exclude' => 0,
            'label' => $lll . '.description',
            'config' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::get('rte'),
            'defaultExtras' => 'richtext:rte_transform[flag=rte_enabled|mode=ts]',
        ],
        'image' => [
            'exclude' => 0,
            'label' => $lll . '.image',
            'config' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::get('imageSingleAltTitle'),
        ],
        'mode' => [
            'exclude' => 1,
            'label' => $lll . '.mode',
            'onChange' => 'reload',
            'config' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::get('pollMode'),
        ],
        'use_cookie_protection' => [
            'displayCond' => 'FIELD:mode:=:public',
            'exclude' => 1,
            'label' => $lll . '.use_cookie_protection',
            'config' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::get('checkbox'),
        ],
        'use_captcha_protection' => [
            'displayCond' => 'FIELD:mode:=:public',
            'exclude' => 1,
            'label' => $lll . '.use_captcha_protection',
            'config' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::get('checkbox'),
        ],
        'without_poll_ticket' => [
            'exclude' => 1,
            'label' => $lll . '.without_poll_ticket',
            'description' => $lll . '.without_poll_ticket_description',
            'onChange' => 'reload',
            'config' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::get('checkbox'),
        ],
        'editable' => [
            'displayCond' => [
                'AND' => [
                    'FIELD:mode:=:frontendUser',
                    'FIELD:without_poll_ticket:REQ:FALSE',
                ],
            ],
            'exclude' => 1,
            'label' => $lll . '.editable',
            'config' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::get('checkbox'),
        ],
        'send_email_on_finish' => [
            'exclude' => 1,
            'label' => $lll . '.send_email_on_finish',
            'config' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::get('checkbox'),
        ],
        'poll_question' => [
            'exclude' => 0,
            'label' => $lll . '.poll_question',
            'config' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::get('pollQuestion'),
        ],
    ],
];

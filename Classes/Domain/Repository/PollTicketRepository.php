<?php

namespace CodingMs\Poll\Domain\Repository;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Poll\Domain\Model\Poll;
use TYPO3\CMS\Extbase\Domain\Model\FrontendUser;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Poll ticket repository
 *
 * @package poll
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class PollTicketRepository extends Repository
{

    /**
     * Find the a poll ticket for a frontend user
     *
     * @param Poll $poll Poll-Object
     * @param FrontendUser $frontendUser FrontendUser-Object
     * @return array|QueryResultInterface
     */
    public function findOneByPollAndFrontendUser(Poll $poll, FrontendUser $frontendUser)
    {
        $query = $this->createQuery();
        $constraints = [];
        $constraints[] = $query->equals('poll', $poll);
        $constraints[] = $query->equals('frontendUser', $frontendUser);
        $query->matching(
            $query->logicalAnd($constraints)
        );
        $query->setLimit(1);
        return $query->execute()->getFirst();
    }

    /**
     * Find the last poll ticket from a frontend user
     *
     * @param FrontendUser $frontendUser FrontendUser-Object
     * @return array|QueryResultInterface
     */
    public function findLastOneFromFrontendUser(FrontendUser $frontendUser)
    {
        $orderings = [
            'uid' => QueryInterface::ORDER_DESCENDING
        ];
        $query = $this->createQuery();
        $query->setOrderings($orderings);
        $constraints = [];
        $constraints[] = $query->equals('frontendUser', $frontendUser);
        $query->matching(
            $query->logicalAnd($constraints)
        );
        $query->setLimit(1);
        return $query->execute()->getFirst();
    }

    /**
     * Find the Poll
     *
     * @param Poll $poll
     * @param boolean $respectStoragePage
     * @return array|QueryResultInterface
     */
    public function findByPoll($poll, $respectStoragePage = true)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage($respectStoragePage);
        $constraints = [];
        $constraints[] = $query->equals('poll', $poll);
        $constraints[] = $query->equals('isFinished', true);
        $query->matching(
            $query->logicalAnd($constraints)
        );
        return $query->execute();
    }

    /**
     * @param array $filter
     * @param boolean $count
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface|int
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function findAllForBackendList(array $filter = array(), $count = false)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $query->getQuerySettings()->setRespectStoragePage(true);
        $query->matching(
            $query->equals('poll', $filter['poll']['selected'])
        );
        if (!$count) {
            if (isset($filter['sortingField']) && $filter['sortingField'] !== '') {
                if ($filter['sortingOrder'] === 'asc') {
                    $query->setOrderings(array($filter['sortingField'] => QueryInterface::ORDER_ASCENDING));
                } else {
                    if ($filter['sortingOrder'] === 'desc') {
                        $query->setOrderings(array($filter['sortingField'] => QueryInterface::ORDER_DESCENDING));
                    }
                }
            }
            if ((int)$filter['limit'] > 0) {
                $query->setOffset((int)$filter['offset']);
                $query->setLimit((int)$filter['limit']);
            }
            return $query->execute();
        } else {
            return $query->execute()->count();
        }
    }

}

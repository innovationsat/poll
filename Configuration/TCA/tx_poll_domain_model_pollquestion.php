<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

$extKey = 'poll';
$table = 'tx_poll_domain_model_pollquestion';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

return [
    'ctrl' => [
        'title' => $lll,
        'label' => 'question_headline',
        'label_alt' => 'question',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'sorting',
        'dividers2tabs' => true,
        'versioningWS' => 2,
        'versioning_followPages' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'type' => 'question_type',
        'hideTable' => 1,
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'iconfile' => 'EXT:poll/Resources/Public/Icons/iconmonstr-help-6.svg'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, question_headline, question, question_type, question_layout, additional_css_class, show_hr_before, question_answer_default, question_answer',
    ],
    'types' => [
        '1' => [
            'showitem' => '
            --palette--;;question_headline_question_type,
            question,
            optional,

        --div--;' . $lll . '.tab_answers,
            question_answer_default, question_answer,

        --div--;' . $lll . '.tab_layout,
            --palette--;;layout'
        ],
        'Single' => [
            'showitem' => '
            --palette--;;question_headline_question_type,
            question,
            optional,

        --div--;' . $lll . '.tab_answers,
            question_answer_default, question_answer,

        --div--;' . $lll . '.tab_layout,
            --palette--;;layout',
            'columnsOverrides' => [
                'question_layout' => [
                    'config' => [
                        'items' =>[
                            ['Layout (normal)', 'Default'],
                            ['Single: Layout (inline mit Skala)', 'InlineScaled'],
                            ['Single: SelectBox', 'SelectBox'],
                        ]
                    ]
                ]
            ]
        ],
        'Multiple' => [
            'showitem' => '
            --palette--;;question_headline_question_type,
            question,
            optional,

        --div--;' . $lll . '.tab_answers,
            question_answer_default, question_answer,

        --div--;' . $lll . '.tab_layout,
            --palette--;;layout',
            'columnsOverrides' => [
                'question_layout' => [
                    'config' => [
                        'items' =>[
                            ['Layout (normal)', 'Default'],
                        ]
                    ]
                ]
            ]
        ],
    ],
    'palettes' => [
        'question_headline_question_type' => ['showitem' => 'question_headline, question_type'],
        'layout' => ['showitem' => 'question_layout, additional_css_class, show_hr_before'],
    ],
    'columns' => [
        'sys_language_uid' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::full('l10n_parent', $table, $extKey),
        'l10n_diffsource' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::full('t3ver_label'),
        'hidden' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::full('hidden'),
        'starttime' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::full('starttime'),
        'endtime' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::full('endtime'),
        'question_headline' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:poll/Resources/Private/Language/locallang_db.xlf:tx_poll_domain_model_pollquestion.question_headline',
            'config' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::get('string'),
        ],
        'question' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:poll/Resources/Private/Language/locallang_db.xlf:tx_poll_domain_model_pollquestion.question',
            'config' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::get('rte'),
            'defaultExtras' => 'richtext:rte_transform[flag=rte_enabled|mode=ts]',
        ],
        'sorting' => [
            'exclude' => 0,
            'label' => $lll . '.sorting',
            'config' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::get('int'),
        ],
        'question_type' => [
            'exclude' => 0,
            'label' => $lll . '.question_type',
            'onChange' => 'reload',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'size' => 1,
                'eval' => 'required',
                'default' => 'Single',
                'items' => [
                    ['Single - Uses only radio-buttons, only a single answer possible', 'Single'],
                    ['Multiple - Uses only checkboxes, multiple answers possible', 'Multiple'],
                ],
            ],
        ],
        'question_layout' => [
            'exclude' => 0,
            'label' => $lll . '.question_layout',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'size' => 1,
                'eval' => 'required',
                'items' => [],
            ],
        ],
        'additional_css_class' => [
            'exclude' => 0,
            'label' => $lll . '.additional_css_class',
            'config' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::get('string'),
        ],
        'show_hr_before' => [
            'exclude' => 1,
            'label' => $lll . '.show_hr_before',
            'config' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::get('checkbox'),
        ],
        'optional' => [
            'exclude' => 1,
            'label' => $lll . '.optional',
            'config' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::get('checkbox'),
        ],
        'question_answer_default' => [
            'exclude' => 0,
            'label' => $lll . '.question_answer_default',
            'displayCond' => [
                'OR' => [
                    'FIELD:question_type:=:Single',
                    'FIELD:question_type:=:SingleWithOptionalUserAnswer',
                    'FIELD:question_type:=:SingleUserAnswer',
                ],
            ],
            'config' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::get('string'),
        ],
        'question_answer' => [
            'exclude' => 0,
            'label' => $lll . '.question_answer',
            'config' => [
                'type' => 'inline',
                'foreign_sortby' => 'sorting',
                'foreign_table' => 'tx_poll_domain_model_pollquestionanswer',
                'foreign_field' => 'poll_question',
                'maxitems' => 9999,
                'appearance' => [
                    'collapse' => 0,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1,
                    'useSortable' => 1,

                    /* Loeschen, Erstellen, etc Buttons ausblenden */
                    'enabledControls' => [
                        'info' => true,
                        'new' => true,
                        'dragdrop' => true,
                        'sort' => true,
                        'hide' => false,
                        'delete' => true
                    ],
                ],
            ],
        ],
        'poll' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
    ],
];

# Poll Changelog

## 2022-04-29 Release of version 2.2.5

*   [TASK] Move extension icon into public folder
*   [BUGFIX] Fix jQuery usage
*   [BUGFIX] Fix documentation configuration
*   [TASK] Add documentations configuration



## 2021-09-07 Release of version 2.2.4

*   [TASK] Extend cookie lifetime for poll participation's
*   [TASK] Fix typo in a label



## 2021-07-27 Release of version 2.2.3

*   [BUGFIX] Fix fetching poll result - ignore all deleted records



## 2021-07-21 Release of version 2.2.2

*   [BUGFIX] Update TCA configuration for starttime and endtime



## 2021-07-09 Release of version 2.2.1

*   [BUGFIX] Add missing database definition for sorting



## 2021-07-04 Release of version 2.2.0

*   [TASK] Refactoring of the saving logic
*   [FEATURE] Add optional-checkbox in all question types
*   [TASK] Add pre-selected answer for SingleWithOptionalUserAnswer
*   [TASK] Optimize backend title for questions
*   [TASK] Add additional documentation for plugins



## 2021-06-30 Release of version 2.1.1

*   [TASK] Add missing EN translations.



## 2021-06-29 Release of version 2.1.0

*   [!!!] This version changes the database structure.
*   [FEATURE] Make poll records sortable and add sorting option to list plugin (manual sorting, crdate or title)



## 2021-06-25 Release of version 2.0.9

*   [TASK] Enable default answer text for SingleUserAnswer



## 2021-05-21 Release of version 2.0.8

*   [BUGFIX] Fix links in documentation
*   [TASK] TCA clean up - remove unused sorting configuration



## 2021-05-15 Release of version 2.0.7

*   [TASK] Add extension key in composer.json
*   [TASK] TS config clean up
*   [TASK] Add documentation notice about user answers



## 2020-10-08 Release of version 2.0.6

*   [BUGFIX] Frontend user check from all PIDs
*   [TASK] Plugin translations
*   [BUGFIX] Frontend user check



## 2020-09-04 Release of version 2.0.5

*   [TASK] Added description text to answer description rte
*   [TASK] Optimize extension description for better visibility in TER



## 2020-08-27 Release of version 2.0.4

*   [BUGFIX] Remove unimplemented combinations of question type and layout



## 2020-07-20 Release of version 2.0.3

*   [BUGFIX] Fix message translations in Poll controller
*   [TASK] Add links to bottom of readme files



## 2020-07-06 Release of version 2.0.2

*   [TASK] Add documentation translations
*   [TASK] Add required namespace for Extbase annotation in domain model



## 2020-06-24 Release of version 2.0.1

*   [TASK] Update documentation
*   [TASK] Update extension description



## 2020-06-24 Release of version 2.0.0

*   [BUGFIX] Fix a bug where answers weren't saved correctly
*   [TASK] Add language label translations
*   [TASK] Poll result refactoring
*   [TASK] Migrate field anonym to without_poll_ticket
*   [TASK] Poll teaser refactoring
*   [TASK] Clean up TCA and Models
*   [TASK] Clean up constants.typoscript file and add labels
*   [TASK] Add Pagetree icon and new content element wizards see #1 and #2
*   [TASK] Refactoring of ext_tables.php
*   [TASK] Preparations for version 2.0.0
*   [TASK] Remove Debug service
*   [TASK] Remove inject annotations and migration lazy annotations



## 2019-10-13  Release of version 1.2.0

*	[TASK] Add Gitlab-CI configuration.



## 2017-11-14  Release of version 1.1.0

*	[BUGFIX] Fixing a poll relation issue. Removing poll_ticket from poll.
*   [FEATURE] Adding frontend admin reset poll feature
*   [TASK] FlashMessage 8.7 Migration
*	[BUGFIX] Fixing a poll saving bug - reset the save service after each iteration
*	[TASK] Add a finish flag in poll ticket that indicates that a poll ticket has been finished

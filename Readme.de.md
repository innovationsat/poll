# Poll: Umfragen für TYPO3

## Features der Basis-Version

*   Anlegen beliebig vieler Umfragen mit beliebig vielen Fragen
*   Beliebig viele Antwortmöglichkeiten, bestehend aus Texten und Bildern
*   Plugin zum Anzeigen einer Umfrage inkl. Ausfüllmöglichkeit und optionalem Ergebnis am Ende
*   Plugin zum Anzeigen von Umfrage-Teasern (bspw. für die Startseite oder Sidebar)
*   Plugin zum Anzeigen aller Umfragen in einer Liste
*   Separates Plugin für das Ergebnis der Umfrage
*   Einfache Umfragen (mehrere Antworten je Frage; eine oder mehrere Antworten)
*   Mehrfachteilnahmen können mittels Cookie unterbunden werden

## Features der PRO-Version

*   Backend-Modul, mit dem sich Umfragen und Umfrageergebnisse verwalten lassen
*   Ergebnisse einer Umfrage als CSV exportieren
*   Benutzerdefinierte Antworten: Benutzer können auch eigene Antworten auf Fragen eingeben
*   Umfrage zurücksetzen

## Roadmap
*   Statistik
*   Spamschutz mittels Captcha
*   Dashboard Widgets
*   Versenden der Ergebnisse per E-Mail

### Links

*   [TYPO3 Poll Produktdetails][link-typo3-poll-product-details]
*   [TYPO3 Poll Dokumentation][link-typo3-poll-documentation]



[link-typo3-poll-product-details]: https://www.coding.ms/de/typo3-extensions/typo3-poll/ "TYPO3 Poll Produktdetails"
[link-typo3-poll-documentation]: https://www.coding.ms/de/dokumentation/typo3-poll/ "TYPO3 Poll Dokumentation"

<?php

namespace CodingMs\Poll\Controller;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Poll\Domain\Model\PollQuestionAnswer;
use CodingMs\Poll\Domain\Model\PollTicket;
use CodingMs\Poll\Domain\Repository\PollQuestionAnswerRepository;
use CodingMs\Poll\Domain\Repository\PollRepository;
use CodingMs\Poll\Domain\Repository\PollTicketRepository;
use CodingMs\Poll\Service\Save\AbstractSave;
use CodingMs\Poll\Service\SendEmailOnFinishService;
use CodingMs\Poll\Service\Validate\AbstractValidate;
use CodingMs\Poll\Utility\PollUtility;
use Exception;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use CodingMs\Poll\Domain\Model\Poll;
use CodingMs\Poll\Domain\Model\PollQuestion;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;
use TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException;
use TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException;
use TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException;
use TYPO3\CMS\Extbase\Mvc\Exception\StopActionException;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use CodingMs\Poll\Utility\AuthorizationUtility;

/**
 *
 * @package poll
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 * @noinspection PhpUnused
 *
 */
class PollController extends ActionController
{

    /**
     * @var PollRepository
     */
    protected $pollRepository;

    /**
     * @param PollRepository $pollRepository
     * @noinspection PhpUnused
     */
    public function injectPollRepository(PollRepository $pollRepository)
    {
        $this->pollRepository = $pollRepository;
    }

    /**
     * @var PollQuestionAnswerRepository
     */
    protected $pollQuestionAnswerRepository;

    /**
     * @param PollQuestionAnswerRepository $pollQuestionAnswerRepository
     * @noinspection PhpUnused
     */
    public function injectPollQuestionAnswerRepository(PollQuestionAnswerRepository $pollQuestionAnswerRepository)
    {
        $this->pollQuestionAnswerRepository = $pollQuestionAnswerRepository;
    }

    /**
     * @var PollTicketRepository
     */
    protected $pollTicketRepository;

    /**
     * @param PollTicketRepository $pollTicketRepository
     * @noinspection PhpUnused
     */
    public function injectPollTicketRepository(PollTicketRepository $pollTicketRepository)
    {
        $this->pollTicketRepository = $pollTicketRepository;
    }

    /**
     * @var PollUtility
     */
    protected $pollUtility;

    /**
     * @param PollUtility $pollUtility
     * @noinspection PhpUnused
     */
    public function injectPollUtility(PollUtility $pollUtility)
    {
        $this->pollUtility = $pollUtility;
    }

    /**
     * Message array for validation
     * @var array
     */
    protected $messages = [];

    /**
     * @var array
     */
    protected $content = [];

    /**
     * Initialize view
     * @param ViewInterface $view
     */
    public function initializeView(ViewInterface $view)
    {
        parent::initializeView($view);
        //
        // Get content data
        $this->content = $this->configurationManager->getContentObject()->data;
        $this->view->assign('content', $this->content);
    }

    /**
     * Shows a list of poll objects
     *
     * @return void
     */
    public function listAction()
    {
        $orderField = in_array($this->settings['listOrderingField'], ['sorting', 'title', 'crdate']) ? $this->settings['listOrderingField'] : 'crdate';
        $orderDirection = ($this->settings['listOrderingDirection'] == QueryInterface::ORDER_DESCENDING) ? QueryInterface::ORDER_DESCENDING : QueryInterface::ORDER_ASCENDING;
        $polls = $this->pollRepository->findAllForList((int)$this->settings['listOffset'], $orderField, $orderDirection);
        //
        // Check page uid for details
        if((int)$this->settings['showPollPid'] > 0) {
            $this->settings['detail']['defaultPid'] = (int)$this->settings['showPollPid'];
        }
        $this->view->assign('settings', $this->settings);
        $this->view->assign('polls', $polls);
    }

    /**
     * action showPollGroup
     *
     * @param array $answers
     * @return void
     */

    /**
     * @param array $answers
     * @throws IllegalObjectTypeException
     * @throws NoSuchArgumentException
     * @throws UnknownObjectException
     * @throws StopActionException
     * @throws Exception
     */
    public function showAction(array $answers = [])
    {
        // Get poll from plugin settings.
        // If a poll in plugin selected, only show that poll and no passed ones!
        $poll = null;
        $pollUid = (int)$this->settings['pollUid'];
        if ($pollUid > 0) {
            $poll = $this->pollRepository->findByIdentifier($pollUid);
        }

        if (!($poll instanceof Poll) && $this->request->hasArgument('poll')) {
            $pollUid = (int)$this->request->getArgument('poll');
            if ($pollUid > 0) {
                $poll = $this->pollRepository->findByIdentifier($pollUid);
            }
        }

        if (!($poll instanceof Poll)) {
            $this->addFlashMessage('Please select a poll', 'Error', AbstractMessage::ERROR);
        } else {
            // Get frontendUser, if required
            $frontendUser = null;


            if ($poll->getMode() == 'frontendUser') {
                try {
                    $frontendUser = $this->pollUtility->getFrontendUser();
                } catch (Exception $exception) {
                    $this->addFlashMessage(
                        $this->getErrorMessageNotAccessible(),
                        '',
                        AbstractMessage::ERROR
                    );
                    return;
                }
                if ($frontendUser === null) {
                    // In case the user pressed the logout button, there will be no user anymore
                    // therefore we do nothing!
                    $this->addFlashMessage(
                        $this->getErrorMessageNotAccessible(),
                        '',
                        AbstractMessage::INFO
                    );
                    return;
                }
            }
            // Fetch questions
            $questions = $poll->getPollQuestion();
        }

        if ($poll instanceof Poll) {
            // Validate poll answers
            $answersArray = [];
            if ($this->request->hasArgument('finishPoll')) {
                $answersArray = $this->validatePollData($answers, $questions);
            } else {
                // or try to restore a editable poll ticket
                if ($poll->getMode() == 'frontendUser' && $poll->isEditable()) {
                    //
                    // Enforce saving?!
                    if ($this->request->hasArgument('savePoll')) {
                        // Write answers into poll ticket
                        $pollTicket = $this->savePollTicket($answers, $questions, $poll, false);
                        $this->addFlashmessage(
                            LocalizationUtility::translate(
                                'tx_poll_message.poll_saved',
                                'Poll'
                            ),
                            '',
                            FlashMessage::OK
                        );
                        $this->redirect('show', 'Poll', null, ['poll' => $pollUid]);
                    } // restore answer data
                    else {
                        $answersArray = $this->pollUtility->getPollTicketAnswersArray($poll, $frontendUser);
                        if (!empty($answersArray)) {
                            $this->addFlashmessage(
                                LocalizationUtility::translate(
                                    'tx_poll_message.poll_successfully_recovered',
                                    'Poll'
                                ),
                                '',
                                FlashMessage::INFO
                            );
                        }
                    }
                }
            }
        }

        // Build Result-URI
        $resultPagePid = (int)$this->settings['result']['defaultPid'];
        if ($resultPagePid === 0) {
            $resultPagePid = (int)$GLOBALS["TSFE"]->id;
        }
        if ($poll instanceof Poll) {
            $uriBuilder = $this->controllerContext->getUriBuilder();
            $resultUri = $uriBuilder->reset()
                ->setCreateAbsoluteUri(true)
                ->setTargetPageUid($resultPagePid)
                ->uriFor(
                    'result',
                    [
                        'poll' => $poll->getUid(),
                        'alreadyParticipated' => 1
                    ]
                );
        } else {
            $resultUri = '';
        }


        /**
         * @todo captcha
        // ..und wenn das Captcha richtig eingegeben wurde!
        $captchaFailed = false;
        if ($poll->getUseCaptchaProtection()) {
            if ($this->request->hasArgument('unusedString') && !$this->checkCaptcha()) {
                $captchaFailed = true;
                $this->messages[0] = 'Bitte übertragen Sie das Captcha korrekt!';
                $this->addFlashmessage('Bitte übertragen Sie das Captcha korrekt!', '',
                    FlashMessage::ERROR);
            }
        }
         * */

        // If there are no error messages, the poll can be saved
        if ($poll instanceof Poll) {
            if (empty($this->messages) && !empty($answers)) {
                // Send the result by E-Mail, if required
                if ($poll->isSendEmailOnFinish()) {
                    /**
                     * @todo check result mail
                     */
                    $sendEmailOnFinishService = $this->objectManager->get(SendEmailOnFinishService::class);
                    if (!$sendEmailOnFinishService->sendEmail($answers, $this->settings)) {
                        throw new Exception('Couldn\'t send E-Mail');
                    }
                }
                // Save poll without poll tickets
                if ($poll->isWithoutPollTicket()) {
                    $this->savePollWithoutTicket($answers, $questions, $poll);
                } else {
                    // ..otherwise write poll into poll ticket
                    $pollTicket = $this->savePollTicket($answers, $questions, $poll, true);
                }
                $this->redirectToUri($resultUri);
            }
        }

        // Soll nur das Ergebnis angezeigt werden?
        // Aber nur wenn Result-Seite die gleiche ist
        // wie die Anzeige-Seite.
        if ($poll instanceof Poll) {
            $showResultOnly = false;
            if ((int)$this->settings['result']['defaultPid'] === (int)$GLOBALS["TSFE"]->id) {
                if ($this->request->hasArgument('alreadyParticipated')) {
                    if ($this->request->getArgument('alreadyParticipated') == 'true') {
                        $showResultOnly = true;
                    }
                }
            }
            // Check if the poll is already answered
            $alreadyParticipated = $this->pollUtility->getAlreadyParticipated($poll, $frontendUser);
            if (!$alreadyParticipated && $this->request->hasArgument('alreadyParticipated')) {
                // This case is required for non cookie-protected polls
                // Otherwise we're not able to show the result afterwards
                if (!$poll->getUseCookieProtection()) {
                    $alreadyParticipated = true;
                }
            }
            $this->view->assign('alreadyParticipated', $alreadyParticipated);

            // In case of already participated..
            if ($alreadyParticipated) {
                // ..show only the result
                $showResultOnly = true;
            } else {
                $showResultOnly = false;
            }
        }

        if ($poll instanceof Poll) {
            // Ergebnis auch anzeigen?!
            // Nur wenn:
            // Entsprechend durch Haken gekennzeichnet,
            // oder Teilnahme und Ergebnis auf einer Seite.
            if ($this->settings['showPollResultBelow'] || $showResultOnly) {
                $this->view->assign('result', $poll->getPollResultArray());
            }
            $this->view->assign('showResultOnly', $showResultOnly);
            $this->view->assign('answers', $answersArray);
            $this->view->assign('questions', $questions);
            $this->view->assign('poll', $poll);
            $this->view->assign('resultUri', $resultUri);
            $this->view->assign('pollTickets', $this->pollTicketRepository->findByPoll($poll));
        }
        $this->view->assign('messages', $this->messages);
        $this->view->assign('settings', $this->settings);
        $this->view->assign('isAdmin', AuthorizationUtility::backendLoginExists());
    }

    /**
     * Captcha check - in case of captcha is required
     * @return bool
     * @throws NoSuchArgumentException
     */
    protected function checkCaptcha()
    {
        /**
         * @todo remove
         */
        $captchaSuccess = false;

        // Captcha-String ermitteln
        $captchaSession = -1;
        if (ExtensionManagementUtility::isLoaded('captcha')) {
            session_start();
            $captchaSession = $_SESSION['tx_captcha_string'];
            $_SESSION['tx_captcha_string'] = '';
        }

        // Captcha-String aus Formular
        $captchaForm = '';
        if ($this->request->hasArgument('unusedString')) {
            $captchaForm = $this->request->getArgument('unusedString');
        }

        // Captcha pruefen
        if ($captchaSession === -1) {
            // Captcha wird nicht verwendet
            // ..also ist alles ok
            $captchaSuccess = true;
        } else {
            if (trim($captchaForm) != '' && trim($captchaForm) == trim($captchaSession)) {
                // Captcha wurde eingegeben und stimmt ueberein
                // ..also ist alles ok
                $captchaSuccess = true;
            }
        }

        return $captchaSuccess;
    }

    /**
     * Teaser of a single poll
     *
     * @return void
     */
    public function teaserAction()
    {
        //
        // A poll was selected in plugin?
        $poll = null;
        $pollUid = (int)$this->settings['pollUid'];
        if($pollUid > 0) {
            $poll = $this->pollRepository->findOneByUid($this->settings['pollUid']);
        }
        if (!($poll instanceof Poll)) {
            //
            // No poll is selected in plugin, try to find the latest
            $poll = $this->pollRepository->findLatest();
            $this->settings['display'] = 'latest';
        }
        else {
            $this->settings['display'] = 'selected';
        }
        //
        // Check page uid for details
        if((int)$this->settings['showPollPid'] > 0) {
            $this->settings['detail']['defaultPid'] = (int)$this->settings['showPollPid'];
        }
        $this->view->assign('settings', $this->settings);
        $this->view->assign('poll', $poll);
    }

    /**
     * Validate a poll - check if all questions are answered
     * @param array $answersIn
     * @param ObjectStorage $questions Questions array
     * @return array
     */
    protected function validatePollData(array $answersIn, ObjectStorage $questions)
    {
        $this->messages = [];
        $answersOut = [];
        if (!empty($questions)) {
            /** @var AbstractValidate[] $validators */
            $validators = [];
            /** @var $question PollQuestion */
            foreach ($questions as $question) {
                // Identify question type
                $questionType = $question->getQuestionType();
                // Initialize validator
                if (!isset($validators[$questionType])) {
                    $objectClass = 'CodingMs\\Poll\\Service\\Validate\\Validate' . $questionType;
                    $validators[$questionType] = $this->objectManager->get($objectClass);
                }
                // Validate question
                $validators[$questionType]->validate($answersIn, $answersOut, $question, $this->messages);
            }
        }
        // Add messages
        if (!empty($this->messages)) {
            $translationKey = 'tx_poll_message.error_poll_validation_fails';
            $message = LocalizationUtility::translate($translationKey, 'Poll');
            $this->addFlashmessage($message, '', FlashMessage::ERROR);
            //
            $this->messages = array_map(function($value) {
                return str_replace('p>', 'i>', $value);
            }, $this->messages);
        }
        return $answersOut;
    }

    /**
     * Save participation in a poll ticket
     *
     * @param array $answersArray Answers array
     * @param ObjectStorage $questions Questions array
     * @param Poll $poll Related Poll
     * @param bool $finish Poll should be marked as finish
     * @return PollTicket
     * @throws Exception
     * @throws IllegalObjectTypeException
     */
    protected function savePollTicket(array $answersArray, ObjectStorage $questions, Poll $poll, $finish = false)
    {
        $persistMode = 'add';

        // FrontendUser required
        if ($poll->getMode() == 'frontendUser') {

            // If poll is already done and not editable
            $frontendUser = $this->pollUtility->getFrontendUser();
            $alreadyDone = $this->pollUtility->isPollAlreadyDoneByFrontendUser($poll, $frontendUser);
            if (!$poll->isEditable() && $alreadyDone) {
                throw new Exception('Poll isn\'t editable and already done by the logged in FE-User');
            } else {
                // Overwrite object
                $pollTicket = $this->pollTicketRepository->findOneByPollAndFrontendUser($poll, $frontendUser);
                if (!($pollTicket instanceof PollTicket)) {
                    // In case of no poll ticket was found, create a new
                    $pollTicket = new PollTicket();
                    $pollTicket->setPoll($poll);
                } else {
                    $persistMode = 'update';
                }
                $pollTicket->setFrontendUser($frontendUser);
            }
        } else {
            // Create a new ticket for answers
            $pollTicket = new PollTicket();
            $pollTicket->setPoll($poll);
        }

        // Set finished on this position
        // because poll ticket could be new or reloaded
        $pollTicket->setIsFinished($finish);

        // Add or update poll ticket
        if ($persistMode == 'add') {
            $this->pollTicketRepository->add($pollTicket);
        } else {
            $this->pollTicketRepository->update($pollTicket);
        }

        /** @var PersistenceManager $persistenceManager */
        $persistenceManager = $this->objectManager->get(PersistenceManager::class);
        $persistenceManager->persistAll();

        // now save all questions
        if (!empty($questions)) {

            /** @var AbstractSave[] $save */
            $save = [];

            /** @var $question PollQuestion */
            // Process each question
            foreach ($questions as $question) {
                // Identify question type
                $qtype = $question->getQuestionType();
                // Initialize saving service
                // which depends on the question type
                if (!isset($save[$qtype])) {
                    $save[$qtype] = $this->objectManager->get('CodingMs\\Poll\\Service\\Save\\Save' . $qtype);
                }

                // Validate question
                /** @todo: hier muss auch update statt insert  gemacht werden */
                $pollTicket = $save[$qtype]->save($pollTicket, $answersArray, $question);
                $save[$qtype]->reset();
            }
        }

        $this->pollTicketRepository->update($pollTicket);

        // protect with cookie
        if ($poll->getUseCookieProtection()) {
            setcookie('pollParticipate_' . $poll->getUid(), 1, time()+60*60*24*1000);
        }
        return $pollTicket;
    }

    /**
     * Save a poll without a ticket.
     * Only valid for Questions of type: Single and Multiple.
     * In this case a counter in answer record is simply increased.
     *
     * @param array $answers Array with answer data from frontend
     * @param ObjectStorage $questions Questions array
     * @param Poll $poll
     * @return bool
     * @throws IllegalObjectTypeException
     * @throws UnknownObjectException
     */
    protected function savePollWithoutTicket(array $answers, ObjectStorage $questions, Poll $poll)
    {
        if (!empty($questions)) {
            /** @var $question PollQuestion */
            foreach ($questions as $question) {
                // Question data
                $qid = $question->getUid();
                $qtype = $question->getQuestionType();
                switch ($qtype) {
                    // Only a single answer
                    // Only radiobuttons
                    case 'Single':
                        // If the answer is available
                        if (isset($answers['question_' . $qid])) {
                            // Example for the radio button:
                            // <input type="radio" name="tx_poll_poll[answers][question_34]" value="174">
                            // This means, in $answers['question_34'] the value 174 is delivered.
                            // The 34 represents the uid of the question.
                            // The 174 represents the uid of the question answer.
                            $answerUid = (int)$answers['question_' . $qid];
                            $pollQuestionAnswer = $this->pollQuestionAnswerRepository->findByUid($answerUid);
                            if ($pollQuestionAnswer instanceof PollQuestionAnswer) {
                                $pollQuestionAnswer->incrementAnswerCount();
                                $this->pollQuestionAnswerRepository->update($pollQuestionAnswer);
                            }
                        }
                        break;
                    // Multiple answers
                    // Only Checkboxes
                    case 'Multiple':
                        // Get all question answers
                        // Because the user might have multiple answers selected.
                        $questionAnswers = $question->getQuestionAnswer();
                        if (!empty($questionAnswers)) {
                            /** @var $questionAnswer PollQuestionAnswer */
                            foreach ($questionAnswers as $questionAnswer) {
                                // ..and check if they are selected..
                                $qaid = $questionAnswer->getUid();
                                if (isset($answers['question_' . $qid . '_' . $qaid])) {
                                    // Example for the checkboxes:
                                    // <input type="checkbox" name="tx_poll_poll[answers][question_33_172]" value="172">
                                    // This means, in $answers['question_33_172'] the value 172 is delivered.
                                    // In case of an unselected checkbox, the parameter is available, but empty!
                                    // The 33 represents the uid of the question.
                                    // The 172 represents the uid of the question answer.
                                    $answerUid = (int)$answers['question_' . $qid . '_' . $qaid];
                                    $pollQuestionAnswer = $this->pollQuestionAnswerRepository->findByUid($answerUid);
                                    if ($pollQuestionAnswer instanceof PollQuestionAnswer) {
                                        $pollQuestionAnswer->incrementAnswerCount();
                                        $this->pollQuestionAnswerRepository->update($pollQuestionAnswer);
                                    }
                                }
                            }
                        }
                        break;
                }
            }
        }
        // Remind participation in cookie
        if ($poll->getUseCookieProtection()) {
            setcookie('pollParticipate_' . $poll->getUid(), 1, time()+60*60*24*1000);
        }
        return true;
    }

    /**
     * Display the result of a poll
     * @noinspection PhpUnused
     * @throws NoSuchArgumentException
     * @throws Exception
     */
    public function resultAction()
    {
        $poll = null;
        if($this->request->hasArgument('poll')) {
            $pollUid = (int)$this->request->getArgument('poll');
            $poll = $this->pollRepository->findByIdentifier($pollUid);
        }
        if(!($poll instanceof Poll)) {
            $this->addFlashMessage('Poll not found.', 'Error!', AbstractMessage::ERROR);
            //
            // Check if we need to redirect to a special page
            if ($this->settings['result']['notFoundPid'] > 0) {
                $uriBuilder = $this->controllerContext->getUriBuilder();
                $uriBuilder->reset()
                    ->setTargetPageUid($this->settings['result']['notFoundPid'])
                    ->setArguments([])
                    ->setCreateAbsoluteUri(true);
                if ((int)TYPO3_version < 10) {
                    $uriBuilder->setUseCacheHash(true);
                }
                $uri = $uriBuilder->build();
                header('Location: ' . $uri);
                exit;
            }
        }
        else {
            if($this->request->hasArgument('alreadyParticipated')) {
                if($this->request->getArgument('alreadyParticipated') === '1') {
                    $this->addFlashmessage(
                        LocalizationUtility::translate(
                            'tx_poll_message.poll_successfully_finished',
                            'Poll'
                        ),
                        '',
                        FlashMessage::OK
                    );
                }
            }
            $this->view->assign('isAdmin', AuthorizationUtility::backendLoginExists());
            $this->view->assign('result', $poll->getPollResultArray());
        }
    }

    /**
     * gets the error message for "poll not accessible"
     * if PID of Login is given (Constants: themes.configuration.pages.login), also create a link to the login page with a redirect URL to the current page
     * @param $separator
     * @return string
     */
    public function getErrorMessageNotAccessible($separator="<br />")
    {
        //Prepare error message
        $errMsg=[
            LocalizationUtility::translate(
                'tx_poll_message.info_poll_no_accessible',
                'Poll'
            )
        ];
        if(!empty($this->settings['loginPid'])) {
            $uriBuilder = $this->controllerContext->getUriBuilder();
            $currentUri=$uriBuilder->getRequest()->getRequestUri();
            $resultUri=$uriBuilder->reset()
                                    ->setCreateAbsoluteUri(true)
                                    ->setTargetPageUid(intval($this->settings['loginPid']))
                ->setArguments(['redirect_url'=>$currentUri])->build();
            $errMsg[]='<a href="'.$resultUri.'">'.
                LocalizationUtility::translate(
                    'tx_poll_message.info_poll_no_accessible.login',
                    'Poll').
                '</a>';
        }
        return implode($separator, $errMsg);
    }

}
